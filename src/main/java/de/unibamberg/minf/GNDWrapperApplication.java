package de.unibamberg.minf;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.unibamberg.minf.gnd.Downloader;
import de.unibamberg.minf.gnd.GNDDump;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class GNDWrapperApplication implements CommandLineRunner {

	//public static void main(String[] args) {
	//	SpringApplication.run(GNDWrapperApplication.class, args);
	//}



    private Map<String, GNDDump> getGNDWrappers(String dumpPath, String mappingPath) {
        Map<String, GNDDump> gndWrappers = new HashMap<>();

        // Geografikum
        String dumpName = "geografikum";
        String url = Downloader.GEOGRAFIKUM_URL;
        String gzFileName = Downloader.GZ_GEOGRAFIKUM_FILENAME;
        String jsonldPath = Downloader.JSONLD_GEOGRAFIKUM_FILENAME;
        gndWrappers.put(dumpName, new GNDDump(dumpName, dumpPath, mappingPath, url, gzFileName, jsonldPath));

        // Koerperschaft
        dumpName = "koerperschaft";
        url = Downloader.KOERPERSCHAFT_URL;
        gzFileName = Downloader.GZ_KOERPERSCHAFT_FILENAME;
        jsonldPath = Downloader.JSONLD_KOERPERSCHAFT_FILENAME;
        gndWrappers.put(dumpName, new GNDDump(dumpName, dumpPath, mappingPath, url, gzFileName, jsonldPath));

        // Kongress
        dumpName = "kongress";
        url = Downloader.KONGRESS_URL;
        gzFileName = Downloader.GZ_KONGRESS_FILENAME;
        jsonldPath = Downloader.JSONLD_KONGRESS_FILENAME;
        gndWrappers.put(dumpName, new GNDDump(dumpName, dumpPath, mappingPath, url, gzFileName, jsonldPath));

        // Person
        dumpName = "person";
        url = Downloader.PERSON_URL;
        gzFileName = Downloader.GZ_PERSON_FILENAME;
        jsonldPath = Downloader.JSONLD_PERSON_FILENAME;
        gndWrappers.put(dumpName, new GNDDump(dumpName, dumpPath, mappingPath, url, gzFileName, jsonldPath));

        // Sachbegriff
        dumpName = "sachbegriff";
        url = Downloader.SACHBEGRIFF_URL;
        gzFileName = Downloader.GZ_SACHBEGRIFF_FILENAME;
        jsonldPath = Downloader.JSONLD_SACHBEGRIFF_FILENAME;
        gndWrappers.put(dumpName, new GNDDump(dumpName, dumpPath, mappingPath, url, gzFileName, jsonldPath));

        // Werk
        dumpName = "werk";
        url = Downloader.WERK_URL;
        gzFileName = Downloader.GZ_WERK_FILENAME;
        jsonldPath = Downloader.JSONLD_WERK_FILENAME;
        gndWrappers.put(dumpName, new GNDDump(dumpName, dumpPath, mappingPath, url, gzFileName, jsonldPath));

        return gndWrappers;
    }
 
    @Override
    public void run(String... args) throws Exception {
        String path = Paths.get("").toAbsolutePath().toString(); // Current directory
        List<String> entities = null; // null implies all entities
        String mapping = null; // No mapping file by default
        int numThreads = 2;

        // Parse arguments
        for (String arg : args) {
            log.info("Argument: {}", arg);
            if (arg.startsWith("--path=")) {
                path = arg.substring("--path=".length());
            } else if (arg.startsWith("--entities=")) {
                String entitiesArg = arg.substring("--entities=".length());
                entities = Arrays.asList(entitiesArg.split(","));
            } else if (arg.startsWith("--mapping=")) {
                mapping = arg.substring("--mapping=".length());
            } else if (arg.startsWith("--numThreads=")) {
                numThreads = Integer.parseInt(arg.substring("--numThreads=".length()));
            }
        }

        final String pathString = path;
        final String mappingString = mapping;

        // Log the parsed values
        log.info("Path: {}", path);
        if (entities == null) {
            log.info("Entities: All");
        } else {
            log.info("Entities: {}", entities);
        }
        log.info("Mapping: {}", mappingString == null ? "None" : mappingString);

        if (mappingString == null || mappingString.isEmpty()) {
            log.error("Mapping must be given");
            return;
        }

        Map<String, GNDDump> gndWrappers = getGNDWrappers(pathString, mappingString);
        ExecutorService executorService = Executors.newFixedThreadPool(numThreads);
        List<Future<Void>> tasks = new ArrayList<>();

        if (entities == null) {
            // Download all entities
            for (Map.Entry<String, GNDDump> entry : gndWrappers.entrySet()) {
                GNDDump gndDump = entry.getValue();

                // Define tasks for each entity dump
                tasks.add(executorService.submit(() -> {
                    log.info("Dumping {}", entry.getKey());
                    try {
                        gndDump.create();
                    } catch (IOException e) {
                        log.error("Unable to create " + entry.getKey() + " dump", e);
                    }
                    return null;
                }));

            }
        } else {
            // Download only the given entities
            for (String entity : entities) {
                GNDDump gndDump = gndWrappers.get(entity);
                if (gndDump == null) {
                    log.error("Invalid entity: {}", entity);
                    return;
                }

                // Define tasks for each entity dump
                tasks.add(executorService.submit(() -> {
                    log.info("Dumping {}", entity);
                    try {
                        gndDump.create();
                    } catch (IOException e) {
                        log.error("Unable to create " + entity + " dump", e);
                        return null;
                    }
                    return null;
                }));
            }
        }

        // Wait for all tasks to complete
        for (Future<Void> future : tasks) {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                log.error("Thread execution interrupted: " + e.getMessage());
                return;
            }
        }

        // Shut down the executor service
        executorService.shutdown();

        log.info("Successfully dumped JSONs");
        return;
    }

    public static void main(String[] args) {
        log.info("STARTING THE APPLICATION");
        
        // for debugging 
        String mappingArg = "--mapping=C:/Users/ba3ev2/Local Folders/My Local Files/repos/gnd-wrapper/mappings/mapping.json";
        String threadArg = "--numThreads=4";
        String[] arrgs = new String[2];
        arrgs[0] = mappingArg;
        arrgs[1] = threadArg;
        args = arrgs;

        SpringApplication.run(GNDWrapperApplication.class, args);
        log.info("APPLICATION FINISHED");
    }
	

}
