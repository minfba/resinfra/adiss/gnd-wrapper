package de.unibamberg.minf.gnd;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.stream.JsonReader; 
import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Slf4j
public class GNDJSONReader {

    private Gson gson;
    private JsonReader reader;

    public GNDJSONReader(String filePath) {
        // Initialize Gson and JsonReader given the filePath
        gson = new Gson();
        try {
            reader = new JsonReader(new InputStreamReader(new FileInputStream(filePath), StandardCharsets.UTF_8));
            reader.beginArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    public void close() {
        try {
            reader.endArray();
            reader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Map<String, LinkedTreeMap<String, Object>> readNext() throws IOException {
        // Returns the next entries from the JSON-ld file
        if(reader.hasNext()) {
            Object obj = gson.fromJson(reader, Object.class);

            Map<String, LinkedTreeMap<String, Object>> newDocMap = new HashMap<>();
            Map<String, LinkedTreeMap> entries = getPreparedEntry(obj);

            for(Map.Entry<String, LinkedTreeMap> entry : entries.entrySet()) {

                LinkedTreeMap<String, Object> newDoc = new LinkedTreeMap<>();

                // If the entry is a gnd ID...
                if (isGNDId(entry.getKey())) {
                    // ...iterate over the attributes of the entry
                    for (Object attr : entry.getValue().entrySet()) {
                        Map.Entry<String, Object> entryMap = (Map.Entry<String, Object>) attr;
                        // and add the references if they are given
                        Object[] referenced = addReferences(entryMap.getKey(), entryMap.getValue(), entries);
                        // and resolve the key value pairs and nestings
                        Object[] processed = prepareKeyValue(referenced[0].toString(), referenced[1]);

                        newDoc.put(processed[0].toString(), processed[1]);
                    }

                    // the use the gndID as an ID for the document and all other attributes as the value
                    LinkedTreeMap gndIDEntry = (LinkedTreeMap) newDoc.remove("gndIdentifier");
                    String gndID = null;
                    if (gndIDEntry != null)
                        gndID = gndIDEntry.get("@value").toString();
                    else
                        gndID = entry.getKey();

                    newDocMap.put(gndID, newDoc);
                }
            }
            return newDocMap;
        } else
            return null;
    }

    // TODO: necessary?
    private LinkedTreeMap<String, Object> resolveArrays(LinkedTreeMap<String, Object> treeMap) {
        for(Map.Entry<String,Object> entry : treeMap.entrySet()) {
            while (entry.getValue() instanceof List && ((List<?>) entry.getValue()).size() <= 1) {
                entry.setValue(((List<?>) entry.getValue()).get(0));
            }
        }

        return treeMap;
    }

    // TODO: necessary?
    public Map<String, LinkedTreeMap> getPreparedEntry(Object entry) {
        Map<String, LinkedTreeMap> entries = new HashMap<>();
        if (entry instanceof List) {
            for (Object obj2 : (List) entry) {
                if (obj2 instanceof AbstractMap)
                    entries.put((String) ((AbstractMap<?, ?>) obj2).get("@id"), resolveArrays((LinkedTreeMap<String, Object>) obj2));
                else // never occurs
                    log.error("Unexpected object type: " + obj2.getClass());
            }
        } else if(entry instanceof AbstractMap) {
            entries.put((String) ((AbstractMap<?, ?>) entry).get("@id"), resolveArrays((LinkedTreeMap<String, Object>) entry));
        }
        return entries;
    }


    // adds references to the place, referenced by "@id"
    public Object[] addReferences(String key, Object value, Map<String, LinkedTreeMap> entries) {
        // look recursively inside LinkedTreeMaps for references
        if (value instanceof LinkedTreeMap) {
            LinkedTreeMap valuemap = (LinkedTreeMap) value;
            LinkedTreeMap<String, Object> processedEntry = new LinkedTreeMap<>();

            for (Object entry : valuemap.entrySet()) {
                Map.Entry<String, Object> entryMap = (Map.Entry<String, Object>) entry;
                Object[] processed = addReferences(entryMap.getKey(), entryMap.getValue(), entries);
                processedEntry.put((String) processed[0], processed[1]);
            }
            value = processedEntry;

        }

        // look recursively inside Lists for references
        else if (value instanceof List) {
            List<Object> newList = new ArrayList<>();
            for (Object item : (List) value) {
                if (item instanceof LinkedTreeMap) {
                    LinkedTreeMap valuemap = (LinkedTreeMap) item;
                    LinkedTreeMap newValuemap = new LinkedTreeMap();

                    for (Object entry : valuemap.entrySet()) {
                        Map.Entry<String, Object> entryMap = (Map.Entry<String, Object>) entry;
                        Object[] processed = addReferences(entryMap.getKey(), entryMap.getValue(), entries);
                        newValuemap.put(processed[0], processed[1]);
                    }

                    newList.add(newValuemap);
                } else
                    newList.add(item);
            }
            value = newList;
        }

        // if the key is "@id" and the value is not a GND id, look for the reference inside the all entries
        if (key.equals("@id") && !isGNDId(value.toString())) {
            // look for reference...
            LinkedTreeMap reference = entries.get(value.toString());
            if (reference != null) {
                key = "@reference";
                value = reference;
            } else {
                // reference is not found...
                if(value.toString().endsWith("about") || value.toString().startsWith("_:node"))
                    log.warn("Reference \"" + value + "\" not found");
            }
        }

        return new Object[]{key, value};
    }

    public Object[] prepareKeyValue(String key, Object value) {
        if (key.startsWith("http") && key.contains("#")) {
            key = key.split("#")[1];
        }

        if (value instanceof LinkedTreeMap) {
            LinkedTreeMap valuemap = (LinkedTreeMap) value;
            LinkedTreeMap<String, Object> processedEntry = new LinkedTreeMap<>();

            for (Object entry : valuemap.entrySet()) {
                Map.Entry<String, Object> entryMap = (Map.Entry<String, Object>) entry;
                Object[] processed = prepareKeyValue(entryMap.getKey(), entryMap.getValue());
                processedEntry.put((String) processed[0], processed[1]);
            }
            value = processedEntry;

        } else if (value instanceof List) {
            List<Object> newList = new ArrayList();
            for (Object item : (List) value) {
                if (item instanceof LinkedTreeMap) {
                    LinkedTreeMap valuemap = (LinkedTreeMap) item;
                    LinkedTreeMap newValuemap = new LinkedTreeMap();

                    for (Object entry : valuemap.entrySet()) {
                        Map.Entry<String, Object> entryMap = (Map.Entry<String, Object>) entry;
                        Object[] processed = prepareKeyValue(entryMap.getKey(), entryMap.getValue());
                        newValuemap.put(processed[0], processed[1]);
                    }

                    newList.add(newValuemap);
                } else if(item instanceof String) {
                    newList.add(item);
                } else if(item instanceof List) {
                    newList.add(item);
                }
            }
            value = newList;

        }

        return new Object[]{key, value};
    }


    public static boolean isGNDId(String text) {
        if (text.toString().startsWith("https://d-nb.info/gnd/") && !text.toString().endsWith("about"))
            return true;
        return false;
    }

    /*public static void main(String[] args) throws IOException {
        Downloader.DownloadGeografikum();
        Downloader.ExtractGeografikumFile();
        JSONReader r = new JSONReader("geografikum.jsonld");
        GeografikumJSONWriter w = new GeografikumJSONWriter("geografikum_dump.json");
        Map<String, LinkedTreeMap<String, Object>> m = new HashMap<>();

        m = r.readNext();
        while(m != null) {
            w.writeEntries(m);
            System.out.println(m.size() + " entries written");
            m = r.readNext();
        }

        r.close();
        w.close();

    }*/

}
