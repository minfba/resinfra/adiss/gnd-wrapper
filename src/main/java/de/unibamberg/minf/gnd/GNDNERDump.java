package de.unibamberg.minf.gnd;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.internal.LinkedTreeMap;

import de.unibamberg.minf.core.wrapper.mapping.Mapping;
import de.unibamberg.minf.core.wrapper.utils.FileUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GNDNERDump {


    private static void downloadAndExtract(String url, String gzFileName, String jsonldFileName) {
        try {
            Downloader.DownloadFile(url, gzFileName);
        } catch (IOException e) {
            log.error("Could not download file", e);
        }
        Downloader.ExtractFile(gzFileName, jsonldFileName);
        FileUtils.DeleteFile(gzFileName);
    }

    private static FileWriter createNerDumpFileWriter(String nerDumpName) {
        File dumpFile = null;
        try {
            dumpFile = new File(nerDumpName);
            
            if (dumpFile.createNewFile()) {
                log.info("File created: " + dumpFile.getName());
            } else {
                log.info("File already exists.");
            }
        return new FileWriter(dumpFile, StandardCharsets.UTF_8);
        
        } catch (IOException e) {
            log.error("Cannot create NER dump file", e);
            return null;
        }
    }

    private static StringBuilder createNerDumpStringBuilder(Map<String, LinkedTreeMap<String, Object>> m, String prefNamePath, String variantNamePath, String tag) {
        StringBuilder stringBuilder = new StringBuilder();
            
        for (LinkedTreeMap<String, Object> entry : m.values()) {
            String preferredName = (String) Mapping.GetValueByPath(entry, prefNamePath);
            if (preferredName != null) {
                stringBuilder.append(preferredName).append(", "+tag+"\n");
            }
            
            Object variantNames = Mapping.GetValueByPath(entry, variantNamePath);
            if (variantNames instanceof String) {
                stringBuilder.append(variantNames).append(", "+tag+"\n");
            } else if (variantNames instanceof List) {
                for (String variantName : (List<String>) variantNames) {
                    stringBuilder.append(variantName).append(", "+tag+"\n");
                }
            }
        }
        return stringBuilder;
    }


    private static void createGeografikumNERDump() throws IOException {
        downloadAndExtract(Downloader.GEOGRAFIKUM_URL, Downloader.GZ_GEOGRAFIKUM_FILENAME, Downloader.JSONLD_GEOGRAFIKUM_FILENAME);
        
        FileWriter dumpFileWriter = createNerDumpFileWriter("geografikum_ner_dump.txt");

        GNDJSONReader r = new GNDJSONReader(Downloader.JSONLD_GEOGRAFIKUM_FILENAME);
        Map<String, LinkedTreeMap<String, Object>> m = new HashMap<>();

        long count = 0;
        m = r.readNext();
        while(m != null) {
            log.info("read " + m.size() + " entries");
            count += m.size();
            
            StringBuilder stringBuilder = createNerDumpStringBuilder(m, 
                                    "preferredNameForThePlaceOrGeographicName.@value", 
                                    "variantNameForThePlaceOrGeographicName.@value", "LOC");
            dumpFileWriter.write(Normalizer.normalize(stringBuilder.toString(), Normalizer.Form.NFC));
        
            log.info("wrote entries!");
            m = r.readNext();
        }

        log.info("Total entries: " + count);

        dumpFileWriter.close();
        r.close();

        FileUtils.DeleteFile(Downloader.JSONLD_GEOGRAFIKUM_FILENAME);

        log.info("Successfully dumped Geografikum NER-Dump");
    }

    private static void createKoerperschaftNERDump() throws IOException {
        downloadAndExtract(Downloader.KOERPERSCHAFT_URL, Downloader.GZ_KOERPERSCHAFT_FILENAME, Downloader.JSONLD_KOERPERSCHAFT_FILENAME);
        
        FileWriter dumpFileWriter = createNerDumpFileWriter("koerperschaft_ner_dump.txt");

        GNDJSONReader r = new GNDJSONReader(Downloader.JSONLD_KOERPERSCHAFT_FILENAME);
        Map<String, LinkedTreeMap<String, Object>> m = new HashMap<>();

        long count = 0;
        m = r.readNext();
        while(m != null) {
            log.info("read " + m.size() + " entries");
            count += m.size();
            
            StringBuilder stringBuilder = createNerDumpStringBuilder(m, 
                                    "preferredNameForTheCorporateBody.@value", 
                                    "variantNameForTheCorporateBody.@value", "ORG");
            dumpFileWriter.write(Normalizer.normalize(stringBuilder.toString(), Normalizer.Form.NFC));
        
            log.info("wrote entries!");
            m = r.readNext();
        }

        log.info("Total entries: " + count);

        dumpFileWriter.close();
        r.close();

        FileUtils.DeleteFile(Downloader.JSONLD_KOERPERSCHAFT_FILENAME);

        log.info("Successfully dumped Koerperschaft NER-Dump");
    }

    private static void createKongressNERDump() throws IOException {
        downloadAndExtract(Downloader.KONGRESS_URL, Downloader.GZ_KONGRESS_FILENAME, Downloader.JSONLD_KONGRESS_FILENAME);
        
        FileWriter dumpFileWriter = createNerDumpFileWriter("kongress_ner_dump.txt");

        GNDJSONReader r = new GNDJSONReader(Downloader.JSONLD_KONGRESS_FILENAME);
        Map<String, LinkedTreeMap<String, Object>> m = new HashMap<>();

        long count = 0;
        m = r.readNext();
        while(m != null) {
            log.info("read " + m.size() + " entries");
            count += m.size();
            
            StringBuilder stringBuilder = createNerDumpStringBuilder(m, 
                                    "preferredNameForTheConferenceOrEvent.@value", 
                                    "variantNameForTheConferenceOrEvent.@value", "EVENT");
            dumpFileWriter.write(Normalizer.normalize(stringBuilder.toString(), Normalizer.Form.NFC));
        
            log.info("wrote entries!");
            m = r.readNext();
        }

        log.info("Total entries: " + count);

        dumpFileWriter.close();
        r.close();

        FileUtils.DeleteFile(Downloader.JSONLD_KONGRESS_FILENAME);

        log.info("Successfully dumped Kongress NER-Dump");
    }

    private static void createPersonNERDump() throws IOException {
        downloadAndExtract(Downloader.PERSON_URL, Downloader.GZ_PERSON_FILENAME, Downloader.JSONLD_PERSON_FILENAME);
        
        FileWriter dumpFileWriter = createNerDumpFileWriter("person_ner_dump.txt");

        GNDJSONReader r = new GNDJSONReader(Downloader.JSONLD_PERSON_FILENAME);
        Map<String, LinkedTreeMap<String, Object>> m = new HashMap<>();

        long count = 0;
        m = r.readNext();
        String[] splitName = null;
        while(m != null) {
            log.info("read " + m.size() + " entries");
            count += m.size();
            StringBuilder stringBuilder = new StringBuilder();
            for (LinkedTreeMap<String, Object> entry : m.values()) {
                String preferredName = (String) Mapping.GetValueByPath(entry, "preferredNameForThePerson.@value");
                if (preferredName != null) {
                    splitName = preferredName.split(", ");
                    if (splitName.length == 2) {
                        stringBuilder.append(splitName[1]).append(" ").append(splitName[0]).append(", PERSON\n");
                    } else if (splitName.length == 1) {
                        stringBuilder.append(splitName[0]).append(", PERSON\n");
                    } else
                        System.out.println(preferredName);
                }
                
                Object variantNames = Mapping.GetValueByPath(entry, "variantNameForThePerson.@value");
                if (variantNames instanceof String) {
                    splitName = preferredName.split(", ");
                    if (splitName.length == 2) {
                        stringBuilder.append(splitName[1]).append(" ").append(splitName[0]).append(", PERSON\n");
                    } else if (splitName.length == 1) {
                        stringBuilder.append(splitName[0]).append(", PERSON\n");
                    } else
                        System.out.println(variantNames);
                } else if (variantNames instanceof List) {
                    for (String variantName : (List<String>) variantNames) {
                        splitName = variantName.split(", ");
                        if (splitName.length == 2) {
                            stringBuilder.append(splitName[1]).append(" ").append(splitName[0]).append(", PERSON\n");
                        } else if (splitName.length == 1) {
                            stringBuilder.append(splitName[0]).append(", PERSON\n");
                        } else
                            System.out.println(variantName);
                    }
                }
            }

            dumpFileWriter.write(Normalizer.normalize(stringBuilder.toString(), Normalizer.Form.NFC));
            

            log.info("wrote entries!");
            m = r.readNext();
        }

        log.info("Total entries: " + count);

        dumpFileWriter.close();
        r.close();

        FileUtils.DeleteFile(Downloader.JSONLD_PERSON_FILENAME);

        log.info("Successfully dumped Person NER-Dump");
    }

    private static void createWerkNERDump() throws IOException {
        downloadAndExtract(Downloader.WERK_URL, Downloader.GZ_WERK_FILENAME,
                Downloader.JSONLD_WERK_FILENAME);

        FileWriter dumpFileWriter = createNerDumpFileWriter("werk_ner_dump.txt");

        GNDJSONReader r = new GNDJSONReader(Downloader.JSONLD_WERK_FILENAME);
        Map<String, LinkedTreeMap<String, Object>> m = new HashMap<>();

        long count = 0;
        m = r.readNext();
        while (m != null) {
            log.info("read " + m.size() + " entries");
            count += m.size();

            StringBuilder stringBuilder = createNerDumpStringBuilder(m,
                    "preferredNameForTheWork.@value",
                    "variantNameForTheWork.@value", "WORK_OF_ART");
            dumpFileWriter.write(Normalizer.normalize(stringBuilder.toString(), Normalizer.Form.NFC));

            log.info("wrote entries!");
            m = r.readNext();
        }

        log.info("Total entries: " + count);

        dumpFileWriter.close();
        r.close();

        FileUtils.DeleteFile(Downloader.JSONLD_WERK_FILENAME);

        log.info("Successfully dumped Werk NER-Dump");
    }


    public static void main(String[] args) {
        try {
            createGeografikumNERDump();
            createKoerperschaftNERDump();
            createKongressNERDump();
            createPersonNERDump();
            createWerkNERDump();
        } catch (IOException e) {
            log.error("Could not create NER dumps", e);
        }


    }

    
}
