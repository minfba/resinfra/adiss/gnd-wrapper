package de.unibamberg.minf.gnd;

import com.google.gson.internal.LinkedTreeMap;

import java.io.*;
import java.util.*;

import de.unibamberg.minf.core.wrapper.mapping.Mapping;
import de.unibamberg.minf.core.wrapper.utils.FileUtils;
import de.unibamberg.minf.core.wrapper.utils.Utils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GNDDump {

    String dumpName;
    String dumpPath;
    String mappingPath;
    String jsonldPath;
    String url;
    String gzFileName;

    public GNDDump(String dumpName, String dumpPath, String mappingPath, String url, String gzFileName, String jsonldPath) {
        if (dumpPath == null || mappingPath == null || jsonldPath == null || dumpName == null || gzFileName == null) {
            throw new IllegalArgumentException("All parameters must be set!");
        }

        this.dumpName = dumpName;
        this.dumpPath = dumpPath;
        this.mappingPath = mappingPath;
        this.jsonldPath = jsonldPath;
        this.url = url;
        this.gzFileName = gzFileName;
    }

    public void create() throws IOException {

        Downloader.DownloadFile(this.url, this.gzFileName);
        Downloader.ExtractFile(this.gzFileName, this.jsonldPath);
        
        GNDJSONReader r = new GNDJSONReader(this.jsonldPath);

        String dumpFileName = this.dumpPath + "/" + this.dumpName + "_dump-" + Utils.GetTimestamp() + ".json";
        List<Mapping> mappings = Mapping.GetMappingsFromJson(this.mappingPath);

        GNDJSONWriter w = new GNDJSONWriter(dumpFileName, mappings);

        Map<String, LinkedTreeMap<String, Object>> m = new HashMap<>();
        long count = 0;
        m = r.readNext();
        while(m != null) {
            w.writeEntries(m);
            count += m.size();
            log.info("wrote " + m.size() + " entries!");
            m = r.readNext();
        }

        log.info("Total entries: " + count);

        r.close();
        w.close();

        FileUtils.DeleteFile(this.gzFileName);
        FileUtils.DeleteFile(this.jsonldPath);

        log.info("Successfully dumped {}", dumpFileName);
    }
    
    public static void main(String[] args) throws IOException {
        String dumpName = "geografikum";
        String dumpPath = "C:\\Users\\ba3ev2\\Local Folders\\My Local Files\\dumps";
        String mappingPath = "C:\\Users\\ba3ev2\\Local Folders\\My Local Files\\repos\\gnd-wrapper\\mappings\\mapping.json";
        String url = Downloader.GEOGRAFIKUM_URL;
        String gzFileName = Downloader.GZ_GEOGRAFIKUM_FILENAME;
        String jsonldPath = Downloader.JSONLD_GEOGRAFIKUM_FILENAME;

        GNDDump geografikumDump = new GNDDump(dumpName, dumpPath, mappingPath, url, gzFileName, jsonldPath);
        geografikumDump.create();

    }

}


