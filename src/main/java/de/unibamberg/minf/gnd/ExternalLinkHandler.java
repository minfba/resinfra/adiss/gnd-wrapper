package de.unibamberg.minf.gnd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ExternalLinkHandler {
    

    @SuppressWarnings("unchecked")
    public static void handleExternalLink(Map<String, Object> values, String url) {
        String provider = "";
        
        if(url.contains("viaf")) {
            // put in links->viaf
            provider = "viaf";
        } else if (url.contains("wikidata")) {
            // put in links->wikidata
            provider = "wikidata";
        } else if (url.contains("wikipedia")) {
            // put in links->wikipedia
            provider = "wikipedia";
        } else if (url.contains("gnd")) {
            // put in links->gnd
            provider = "gnd";
        } else if (url.contains("geonames")) {
            // put in links->geonames
            provider = "geonames";
        } else if (url.contains("loc")) {
            provider = "library of congress";
        } else if (url.contains("orcid")) {
            provider = "orcid";
        } else {
            // put in links->other
            provider = "other";
        }
        String externalID = de.unibamberg.minf.core.wrapper.utils.Utils.extractIdFromUrl(url);
        Map<String, Object> externalLink = new HashMap<>();
        externalLink.put("provider", provider);
        externalLink.put("id", externalID);
        externalLink.put("url", url);

        if (!values.containsKey("externalLinks")) {
            List<Map<String, Object>> externalLinkList = new ArrayList<>();
            externalLinkList.add(externalLink);
            values.put("externalLinks", externalLinkList);
            return;
        }

        List<Map<String, Object>> externalLinks = (List<Map<String, Object>>) values.get("externalLinks");
        externalLinks.add(externalLink);
    }

}
