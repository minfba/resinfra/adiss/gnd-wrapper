package de.unibamberg.minf.gnd;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.stream.JsonWriter; 


import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.json.simple.JSONObject;

import java.text.Normalizer;

import de.unibamberg.minf.core.wrapper.mapping.Mapping;
import de.unibamberg.minf.core.wrapper.utils.GeoUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GNDJSONWriter {

    private final JsonWriter writer;

    private final Gson gson;

    private List<Mapping> mappings;

    private ObjectMapper mapper = new ObjectMapper();


    public GNDJSONWriter(String filePath) {
        // Initialize Gson and JsonWriter given the filePath
        gson = new Gson();

        try {
            Writer fstream = null;
            fstream = new OutputStreamWriter(new FileOutputStream(new File(filePath)), StandardCharsets.UTF_8);
            writer = new JsonWriter(fstream);
            writer.beginArray();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        mappings = new ArrayList<Mapping>();
    }

    public GNDJSONWriter(String filePath, List<Mapping> mappings) {
        // Initialize Gson and JsonWriter given the filePath
        gson = new Gson();

        try {
            Writer fstream = null;
            fstream = new OutputStreamWriter(new FileOutputStream(new File(filePath)), StandardCharsets.UTF_8);
            writer = new JsonWriter(fstream);
            writer.beginArray();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        this.mappings = mappings;
    }

    // Ends the array of the file and closes the writer
    public void close() {
        try {
            writer.endArray();
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // Stream writes entries to the opened json file
    public void writeEntries(Map<String, LinkedTreeMap<String, Object>> entries) {
        try {
            for (Map.Entry<String, LinkedTreeMap<String, Object>> entry : entries.entrySet()) {
                writer.beginObject();

                // write id
                writer.name("_id");
                writer.value(entry.getKey());

                // write payload
                writer.name("payload");

                // processes some entries to the right format
                LinkedTreeMap<String, Object> values = processEntries(entry.getValue());
                values.put("gndId", entry.getKey());
                // maps the values to the correct keys based on the mapping dictionary
                //values = mapEntries(entry.getValue());
                Map<String, Object> remappedValues = (LinkedTreeMap<String, Object>) Mapping.ReMap(values, mappings);

                // convert values to a json object
                JsonObject jsonObject = gson.toJsonTree(remappedValues).getAsJsonObject();
                // normalize the json object, to resolve umlaut issues
                String json = Normalizer.normalize(jsonObject.toString(), Normalizer.Form.NFC);

                writer.jsonValue(json);

                writer.endObject();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    public LinkedTreeMap<String, Object> processEntries(LinkedTreeMap<String, Object> values) {  
        processGeographicAreaCode(values);
        processMediumOfPerformance(values); // weird shit, but keep it like this until it is really needed
        processExternalIds(values); 
        processCoordinates(values);
        return values;
    }

    
    private void processGeographicAreaCode(LinkedTreeMap<String, Object> values) {
        // remove the preceding url for the geographicAreaCode
        List<String> geoAreaCodes = new ArrayList<>();

        Object geoAreaObject = values.get("geographicAreaCode");
        if (geoAreaObject instanceof LinkedTreeMap) {
            String geoAreaCode = (String) ((LinkedTreeMap<String, Object>) geoAreaObject).get("@id");
            geoAreaCode = geoAreaCode.split("#")[1];
            geoAreaCodes.add(geoAreaCode);
        } else if(geoAreaObject instanceof List) {
            // if there are multiple geographicAreaCodes, add them all to the list
            for (Object geoArea : (List<?>) geoAreaObject) {
                String geoAreaCode = (String) ((LinkedTreeMap<String, Object>) geoArea).get("@id");
                geoAreaCode = geoAreaCode.split("#")[1];
                geoAreaCodes.add(geoAreaCode);
            }
        }

        if (geoAreaCodes.size() == 1)
            values.put("gndGeographicAreaCode", geoAreaCodes.get(0));
        else if(geoAreaCodes.size() > 1)
            values.put("gndGeographicAreaCode", geoAreaCodes);

        // https://www.iso.org/iso-3166-country-codes.html
        // add country codes to the values
        List<String> countryCodes = new ArrayList<>();
        List<String> subdivisionCodes = new ArrayList<>();
        for (String geoCode : geoAreaCodes) {
            String[] codes = geoCode.split("-");
            if (codes.length == 1) {
                countryCodes.add(codes[0]);
            } else if (codes.length >= 2) { // should be continent and country code 
                countryCodes.add(codes[1]);

                if (codes.length >= 3) 
                    subdivisionCodes.add(codes[2]);
            }     
        }

        if (countryCodes.size() == 1)
            values.put("countryCode", countryCodes.get(0));
        else if(countryCodes.size() > 1)
            values.put("countryCode", countryCodes);

        if (subdivisionCodes.size() == 1)
            values.put("subdivisionCode", subdivisionCodes.get(0));
        else if(subdivisionCodes.size() > 1)
            values.put("subdivisionCode", subdivisionCodes);
    }

    private void processMediumOfPerformance(LinkedTreeMap<String, Object> values) {
        // resolve mediumOfPerformance field in Werke (which is pretty weird tbh)
        if (values.containsKey("mediumOfPerformance")) {
            Object mediumOfPerformance = values.get("mediumOfPerformance");
            if (mediumOfPerformance instanceof List) {
                List<String> mediums = new ArrayList<>();
                for (Object mediumObjects : (List<?>) mediumOfPerformance) {
                    if (mediumObjects instanceof LinkedTreeMap) {
                        if(((LinkedTreeMap<?, ?>) mediumObjects).containsKey("@id"))
                            mediums.add(((LinkedTreeMap<?, ?>) mediumObjects).get("@id").toString());
                        if(((LinkedTreeMap<?, ?>) mediumObjects).containsKey("@value"))
                            mediums.add(((LinkedTreeMap<?, ?>) mediumObjects).get("@value").toString());
                    } else if(mediumObjects instanceof String) {
                        mediums.add((String) mediumObjects);
                    } else if(mediumObjects instanceof List) {
                        mediums.addAll((Collection<? extends String>) mediumObjects);
                    }
                }
                if(mediums.size() == 0)
                    values.remove("mediumOfPerformance");
                values.put("mediumOfPerformance", mediums);
            } else if(mediumOfPerformance instanceof LinkedTreeMap<?,?>) {
                if(((LinkedTreeMap<?, ?>) mediumOfPerformance).containsKey("@reference"))
                    values.remove("mediumOfPerformance");
            }
        }
    }

    private void processExternalIds(LinkedTreeMap<String, Object> values) {
        // resolve sameAs links
        if (values.containsKey("sameAs")) {
            Object sameAs = values.get("sameAs");

            if(sameAs instanceof List && ((List<?>) sameAs).size() > 0 && ((List<?>) sameAs).get(0) instanceof LinkedTreeMap) {
                for (LinkedTreeMap linkMap : (List<LinkedTreeMap>)sameAs) {
                    String link = linkMap.get("@id").toString();
                    ExternalLinkHandler.handleExternalLink(values, link);
                }
            } else if (sameAs instanceof LinkedTreeMap<?,?>) {
                for(Object linkobj : ((LinkedTreeMap<?, ?>) sameAs).values()) {
                    String link = linkobj.toString();
                    ExternalLinkHandler.handleExternalLink(values, link);
                }
            } else {
                log.warn("unhandled case for sameAs links!");
            }

        }
    }

    private void processCoordinates(LinkedTreeMap<String, Object> values) {
        String wktLiteral = (String) Mapping.GetValueByPath(values, "hasGeometry.@reference.asWKT.@value");
        if (wktLiteral != null) {
            String geoJson = GeoUtils.wktToGeoJson(wktLiteral, true);
            JSONObject jsonObj = null;
            try {
                jsonObj = mapper.readValue(geoJson, JSONObject.class);
                jsonObj.remove("crs");
                // convert the polygon to a point,
                // gnd fucked up the polygons so far, they are basically useless, so lets just use the first point as a coordinate here
                if (jsonObj.getOrDefault("type", "Point").equals("Polygon")) {
                    List<List<Double[]>> coordinates = (List<List<Double[]>>) jsonObj.get("coordinates");
                    jsonObj.put("type", "Point");
                    jsonObj.put("coordinates", coordinates.get(0).get(0));
                    log.info("Converted Polygon to Point");
                } else if(!jsonObj.get("type").equals("Point")) {
                    log.warn("Unexpected type:" + geoJson);
                }
            } catch (JsonProcessingException e) {
                log.error("Could not parse jsonObject ", e);
            }
            Mapping.SetValueByPath(values, "coordinates.point", jsonObj);
        }
    }



}

