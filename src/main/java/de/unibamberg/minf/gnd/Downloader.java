package de.unibamberg.minf.gnd;

import de.unibamberg.minf.core.wrapper.utils.CompressionUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;

@Slf4j
public class Downloader {

    public final static String GEOGRAFIKUM_URL = "https://data.dnb.de/opendata/authorities-gnd-geografikum_lds.jsonld.gz";
    public final static String GZ_GEOGRAFIKUM_FILENAME = "geografikum.jsonld.gz";
    public final static String JSONLD_GEOGRAFIKUM_FILENAME = "geografikum.jsonld";

    public final static String KOERPERSCHAFT_URL = "https://data.dnb.de/opendata/authorities-gnd-koerperschaft_lds.jsonld.gz";
    public final static String GZ_KOERPERSCHAFT_FILENAME = "koerperschaft.jsonld.gz";
    public final static String JSONLD_KOERPERSCHAFT_FILENAME = "koerperschaft.jsonld";

    public final static String PERSON_URL = "https://data.dnb.de/opendata/authorities-gnd-person_lds.jsonld.gz";
    public final static String GZ_PERSON_FILENAME = "person.jsonld.gz";
    public final static String JSONLD_PERSON_FILENAME = "person.jsonld";

    public final static String WERK_URL = "https://data.dnb.de/opendata/authorities-gnd-werk_lds.jsonld.gz";
    public final static String GZ_WERK_FILENAME = "werk.jsonld.gz";
    public final static String JSONLD_WERK_FILENAME = "werk.jsonld";

    public final static String SACHBEGRIFF_URL = "https://data.dnb.de/opendata/authorities-gnd-sachbegriff_lds.jsonld.gz";
    public final static String GZ_SACHBEGRIFF_FILENAME = "sachbegriff.jsonld.gz";
    public final static String JSONLD_SACHBEGRIFF_FILENAME = "sachbegriff.jsonld";
    
    public final static String KONGRESS_URL = "https://data.dnb.de/opendata/authorities-gnd-kongress_lds.jsonld.gz";
    public final static String GZ_KONGRESS_FILENAME = "kongress.jsonld.gz";
    public final static String JSONLD_KONGRESS_FILENAME = "kongress.jsonld";


    public static void DownloadGeografikum() throws IOException {
        DownloadFile(GEOGRAFIKUM_URL, GZ_GEOGRAFIKUM_FILENAME);
    }
    public static void ExtractGeografikumFile() {
        ExtractFile(GZ_GEOGRAFIKUM_FILENAME, JSONLD_GEOGRAFIKUM_FILENAME);
    }

    public static void DownloadKoerperschaft() throws IOException {
        DownloadFile(KOERPERSCHAFT_URL, GZ_KOERPERSCHAFT_FILENAME);
    }
    public static void ExtractKoerperschaftFile() {
        ExtractFile(GZ_KOERPERSCHAFT_FILENAME, JSONLD_KOERPERSCHAFT_FILENAME);
    }

    public static void DownloadPerson() throws IOException {
        DownloadFile(PERSON_URL, GZ_PERSON_FILENAME);
    }
    public static void ExtractPersonFile() {
        ExtractFile(GZ_PERSON_FILENAME, JSONLD_PERSON_FILENAME);
    }

    public static void DownloadWerk() throws IOException {
        DownloadFile(WERK_URL, GZ_WERK_FILENAME);
    }
    public static void ExtractWerkFile() {
        ExtractFile(GZ_WERK_FILENAME, JSONLD_WERK_FILENAME);
    }

    public static void DownloadSachbegriff() throws IOException {
        DownloadFile(SACHBEGRIFF_URL, GZ_SACHBEGRIFF_FILENAME);
    }
    public static void ExtractSachbegriffFile() {
        ExtractFile(GZ_SACHBEGRIFF_FILENAME, JSONLD_SACHBEGRIFF_FILENAME);
    }

    public static void DownloadKongress() throws IOException {
        DownloadFile(KONGRESS_URL, GZ_KONGRESS_FILENAME);
    }
    public static void ExtractKongressFile() {
        ExtractFile(GZ_KONGRESS_FILENAME, JSONLD_KONGRESS_FILENAME);
    }

    public static void DownloadFile(String url, String filename) throws IOException {
        log.info("Downloading " + url);
        FileOutputStream fos = new FileOutputStream(filename);
        fos.getChannel().transferFrom(Channels.newChannel(new URL(url).openStream()), 0, Long.MAX_VALUE);
        fos.close();
    }

    public static void ExtractFile(String gzFilename, String jsonldFilename) {
        log.info("Extracting \"" + gzFilename + "\" as \"" + jsonldFilename + "\"");
        try {
            CompressionUtils.extractGZipFile(gzFilename, jsonldFilename);
        } catch (IOException e) {
            log.error("Could not extract "+gzFilename, e);
        }
    }

}
