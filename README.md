# GND Wrapper

A commandline wrapper for ADISS to process and remap the data of all gnd entities. 
- Can create json dumps to be indexed by the Generic-Search API
- Can create NER dumps for training a NER model using names from the GND entities



## Create the dumps
To start the dumps process call the jar with the following parameters:
- path: The path to the directory where the dumps should be created (optional: default is the current directory)
- mapping: The path to the mapping file! This must be provided and a valid mapping file must be present!
- numThreads: The number of threads to use for the dumps creation (optional: default is 2)
- entities: The entities (as list) to process (optional: default is all entities). Possible values are: geografikum, koerperschaft, kongress, person, sachbegriff, werk 

Example: 
`java -jar gnd-api-1.5-SNAPSHOT.jar --path="C:/Users/ba3ev2/Local Folders/My Local Files/dumps" --mapping="C:/Users/ba3ev2/Local Folders/My Local Files/repos/gnd-api/mappings/mapping.json" --numThreads=3`








